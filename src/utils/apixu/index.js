import conditionsJSON from './conditions.json';

const APIXU_KEY = 'aa3ccd8fd304472da80223339181907';
const APIXU_BASE_URL = `http://api.apixu.com/v1/forecast.json?key=${APIXU_KEY}&days=7`;

class Apixu {
  static getByCity = (name) => {
    const url = `${APIXU_BASE_URL}&q=${name}`;
    return fetch(url);
  }

  static getByPosition = ({ latitude, longitude }) => {
    const url = `${APIXU_BASE_URL}&q=${latitude},${longitude}`;
    return fetch(url);
  }

  static getConditionsByCode = (code) => {
    let condition;
    condition = conditionsJSON.filter((item) => item.code === code);
    return condition[0];
  }

  static isDay = (period) => (period ? true : false);
}

export default Apixu;