const WORLD_WEATHER_KEY = 'e6d81dfb033f4a8989a25426181907';
const WORLD_WEATHER_URL = 'http://api.worldweatheronline.com/premium/v1/';

class WorldWeather {
  static getByCityName = (name) => {
    const url = WorldWeather._createUrl({
      name: name,
      numberOfDays: 1
    });
    return fetch(url);
  }

  static _createUrl = ({ name, numberOfDays = 7, date = 'yes', includelocation = 'yes', showlocaltime = 'yes', extra = 'yes' }) => {
    return `${WORLD_WEATHER_URL}weather.ashx?key=${WORLD_WEATHER_KEY}&q=${name}
            &format=json&num_of_days=${numberOfDays}
            &date=${date}
            &includelocation=${includelocation}
            &showlocaltime=${showlocaltime}
            &extra=${extra}`;
  }
}

export default WorldWeather;