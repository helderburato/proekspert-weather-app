import moment from 'moment';

export const formatToFullDay = (date) => {
  return moment(date).format('dddd, MMMM Do YYYY');
}

export const formatDateToDay = (date) => {
  return moment(date).format('dddd');
}