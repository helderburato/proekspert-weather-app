import React from 'react';
import PropTypes from 'prop-types';
import Degree from './Degree';

const Weekday = ({ day, icon, degree, degreeF, temperature }) => {
  return (
    <div className="day">
      <h3 className='day__name'>{day}</h3>

      <div className='day__icon__container'>
        <i className={`day__icon wi ${icon}`}></i>
      </div>

      <div className='day__temp'>
        <Degree 
          value={degree} 
          valueF={degreeF}
          temperature={temperature}
        />
      </div>
    </div>
  );
}

Weekday.propTypes = {
  day: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  degree: PropTypes.number.isRequired,
  degreeF: PropTypes.number.isRequired,
  temperature: PropTypes.bool.isRequired,
};

export default Weekday;