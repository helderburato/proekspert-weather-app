import React from 'react';
import PropTypes from 'prop-types';
import { capitalize } from '../utils/string';

const DayDegree = ({ data }) => {
  const renderAstro = (item) => {
    if (!item) return;
    
    let period = [];

    for (let field in item) {
      period.push(
        <div key={field}>
          <span>{capitalize(field)}</span>
          <span>{item[field]}</span>
        </div>
      );
    }
    return period;
  }

  return (
    <div className="temp__day">
      {renderAstro(data)}
    </div>
  );
}

DayDegree.propTypes = {
  data: PropTypes.object.isRequired,
};

export default DayDegree;