import React, { Component } from 'react';
import Degree from './Degree';
import Weekday from './Weekday';
import DayDegree from './DayDegree';
import Alert from './Alert';
import Apixu from '../utils/apixu';
import { formatToFullDay, formatDateToDay } from '../utils/date';

class App extends Component {

  constructor() {
    super();

    this.state = {
      city: '',
      alert: {
        type: '',
        message: '',
      },
      invalidSearch: false,
      showWeather: false,
      data: null,
      temperatureChecked: false,
      disableForm: false,
      loader: false,
    };
  }

  handleSubmit(event) {
    event.preventDefault();

    this.setState({
      disableForm: true,
      loader: true,
    });

    Apixu.getByCity(this.state.city)
    .then(res => res.text())
    .then(data => {
      const parsed = JSON.parse(data);

      this.setState({
        disableForm: false,
        loader: false,
      });

      if (typeof parsed.error !== 'undefined') {
        this.setState({
          city: '',
          data: null,
          showWeather: false,
          invalidSearch: true,
          alert: {
            type: 'error',
            message: (parsed.error.message || 'No results found for searched term.'),
          },
        });
        this.inputCity.focus();
        return;
      }

      this.setState({
        invalidSearch: false,
        alert: {
          type: 'success',
          message: '',
        },
        showWeather: true,
        data: parsed,
      });
    });
  }

  handleChange(event) {
    this.setState({
      city: event.target.value
    });
  }

  handleClickBack(event) {
    event.preventDefault();

    this.setState({
      city: '',
      invalidSearch: false,
      alert: {
        type: 'success',
        message: '',
      },
      showWeather: false,
      data: null,
    });
  }

  handleChangeType() {
    this.setState({
      temperatureChecked: !this.state.temperatureChecked
    });
  }

  handleCurrentPosition(event) {
    event.preventDefault();

    const self = this;

    navigator.geolocation.getCurrentPosition(function (res) {
      if (res) {
        const { latitude, longitude } = res.coords;

        self.setState({
          disableForm: true,
          loader: true,
        });

        Apixu.getByPosition({
          latitude,
          longitude
        })
        .then(res => res.text())
        .then(data => {
          const parsed = JSON.parse(data);

          self.setState({
            disableForm: false,
            loader: false,
          });

          if (typeof parsed.error !== 'undefined') {
            self.setState({
              city: '',
              data: null,
              showWeather: false,
              invalidSearch: true,
              alert: {
                type: 'error',
                message: (parsed.error.message || 'No results found for searched term.'),
              },
            });
            self.inputCity.focus();
            return;
          }

          self.setState({
            invalidSearch: false,
            alert: {
              type: 'success',
              message: '',
            },
            showWeather: true,
            data: parsed,
          });
        });
      }
    });
  }

  renderLoader() {
    if (!this.state.loader) return;

    return <div className="loader"></div>;
  }

  renderAlert() {
    if (!this.state.invalidSearch) return;

    const { type, message } = this.state.alert;

    return (
      <Alert
        type={type}
        message={message}
      />
    );
  }

  renderWeekday(weather) {
    if (!weather) return;

    const { day } = weather;
    let { condition } = day;

    condition = this._mergeConditionsByCode(condition, condition.code);

    return (
      <Weekday
        key={weather.date}
        day={formatDateToDay(weather.date)}
        icon={condition.iconDay}
        degree={day.maxtemp_c}
        degreeF={day.maxtemp_f}
        temperature={this.state.temperatureChecked}
      />
    );
  }

  renderWeather() {
    if (!this.state.showWeather) return;

    const {
      current, 
      forecast,
      location,
    } = this.state.data;
    const { forecastday } = forecast;
    const { astro } = forecastday[0];
    let { condition } = current;

    condition = this._mergeConditionsByCode(condition, condition.code);

    return (
      <div className="weather-container">
        <nav className="navbar">
          <div className="navbar__left">
            <button 
              onClick={(event) => {
                this.handleClickBack(event);
              }}
              className="button button__icon" 
              type="button"
            >
              <i className="icon material-icons">arrow_back</i>
            </button>

            <h1 className="title">{`${location.name}, ${location.country}`}</h1>
          </div>
          
          <div className="navbar__right">
            <label className='switch'>
              <input 
                onChange={(event) => {
                  this.handleChangeType(event);
                }}
                checked={this.state.temperatureChecked}
                className='switch__input' 
                name='switch-degree' 
                type='checkbox'
              />
              <span className="switch__label" data-on={'°C'} data-off={'°F'}></span>
              <span className="switch__handle"></span>
            </label>
          </div>
        </nav>

        <div className="weather-container__wrapper">
          <h2>{formatToFullDay(location.localtime)}</h2>

          <h3>{condition.text}</h3>

          <div className="results">
            <div>
              <h4 className="temp">
                <Degree 
                  value={current.temp_c} 
                  valueF={current.temp_f}
                  temperature={this.state.temperatureChecked}
                />
              </h4>
            </div>
            <div>
              <i className={`temp__icon wi ${Apixu.isDay(current.is_day) ? condition.iconDay : condition.iconNight}`}></i>
            </div>
            <div>
              <DayDegree data={astro} />
            </div>
          </div>

          <div className="week">
            {forecastday.map((weather) => this.renderWeekday(weather))}
          </div>
        </div>
      </div>
    );
  }

  _mergeConditionsByCode(currentCondition, code) {
    let newConditions = Apixu.getConditionsByCode(code);
    return Object.assign(currentCondition, newConditions);
  }

  renderSearch() {
    if (this.state.showWeather) return;

    return (
      <div className="search-location__container">
        <form 
          onSubmit={(event) => {
            this.handleSubmit(event);
          }}
          className="form-location"
        >
          <div className="text__input__container">
            <input 
              value={this.state.city} 
              onChange={(event) => {
                this.handleChange(event);
              }}
              className="text__input" 
              type="text" 
              placeholder="City"
              ref={el => this.inputCity = el}
              disabled={this.state.disableForm}
            />
            <button disabled={this.state.disableForm} className="button button__icon" type="submit">
              <i className="icon material-icons">search</i>
            </button>
          </div>
        </form>
        <div className="text">or</div>
        <p className="text text__position">
          use my <a className='current-position' onClick={(event) => {
            this.handleCurrentPosition(event);
          }}>current position</a>
        </p>

        {this.renderAlert()}
      </div>
    );
  }

  render() {
    return (
      <div className="wrapper">
        <div className="content container">
          {this.renderLoader()}
          {this.renderSearch()}
          {this.renderWeather()}
        </div>
      </div>
    );
  }
}

export default App;
