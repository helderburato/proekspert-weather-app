import React from 'react';
import PropTypes from 'prop-types';

const Degree = ({ value, valueF, className, temperature }) => {
  const renderByType = () => {
    if (!temperature) return <span className={className}>{Math.round(value)}°C</span>;
    return <span className={className}>{Math.round(valueF)}°F</span>
  }

  return renderByType();
};

Degree.defaultProps = {
  className: '',
  temperature: false,
  value: 0,
  valueF: 0,
};

Degree.propTypes = {
  className: PropTypes.string,
  value: PropTypes.number,
  valueF: PropTypes.number,
  temperature: PropTypes.bool,
};

export default Degree;