import React from 'react';
import PropTypes from 'prop-types';

const Alert = ({ message, type }) => {
  return (
    <div className={`alert ${type}`}>
      {message}
    </div>
  );
}

Alert.defaultProps = {
  type: 'success',
};

Alert.propTypes = {
  type: PropTypes.string,
  message: PropTypes.string.isRequired,
};

export default Alert;