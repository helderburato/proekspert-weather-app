import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styl/main.styl';
import App from './components/App';

ReactDOM.render(
  <App />, 
  document.getElementById('root')
);