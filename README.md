# Proekspert Weather App

A simple weather app using React/Redux.

## Requirements

* [NodeJS](https://nodejs.org/en/);
* [Yarn](https://yarnpkg.com/en/).

## Get started

* Clone the repo:
```bash
$ git clone git@bitbucket.org:helderburato/proekspert-weather-app.git
$ git clone https://helderburato@bitbucket.org/helderburato/proekspert-weather-app.git
```

## Install

* Packages:
```bash
$ yarn install
```

## Usage

* Running the project:
```bash
$ npm start
```

## Author

[Helder Burato Berto](https://github.com/helderburato)